# Universidade Federal de Santa Catarina
> Centro Tecnológico Departamento de Informática e Estatística.

# Instalação de dependências

Você precisa instalar o TexLive e o XeLatex para compilar esse documento.

# Compilando

Basta executar a linha de comando abaixo:

`xelatex /path/to/report.tex`

# Objetivos Gerais e Específicos
Permitir ao estudante aplicar, em ambiente profissional, conceitos e práticas
obtidas durante o curso.

1. Conhecer com maior proximidade o ambiente profissional.
+ Adaptar e aplicar os conhecimentos acadêmicos adquiridos no curso.
+ Interagir com profissionais da área ou afins.
+ Adquirir experiência profissional na área de atuação.

# Conteúdo Programático
1. Desenvolvimentos de atividades profissionais ou de pesquisa na área de
conhecimento do estágio.
+ Elaboração de relatórios descrevendo base teórica associada ao tema de
estágio, apresentando diversos aspectos da realização.
+ Fundamentos Teóricos.
+ Metodologia.
+ Procedimentos.
+ Resultados.
+ Relacionamento entre atividades desenvolvidas e a bibliografia associada à
área de atuação do estágio.
+ Metodologia.

# Etapas
+ Etapa 1: Contato inicial com os estudantes através da lista da disciplina.
+ Etapa 2: Fornecimento de modelo e orientações para elaboração do relatório de
estágio.
+ Etapa 3: Recebimento do relatório de estágio pelo professor responsável.
+ Etapa 4: Avaliação dos relatórios e divulgação da Média Final.

# Avaliação
A avaliação será realizada com base na elaboração e entrega de um relatório
descrevendo as atividades realizadas durante o período de estágio. Modelo e
regras de elaboração do mesmo são fornecidas pelo professor responsável.

# Cronograma
+ Etapa 1 - Semana 1 - Dia 6 de Março
+ Etapa 2 - Semana 6 - Dia 14 de Abril
+ Etapa 3 - Semana 15 - Dia 23 de Junho
+ Etapa 4 - Semana 18 - Dia 21 de Julho

# Bibliografia Básica
1. http://www.fao.org/home/en/
+ http://www.fao.org/brasil/pt/
+ http://www.fao.org/nr/water/docs/ReferenceManualV32.pdf
+ http://www.fao.org/docrep/X0490E/x0490e06.htm
+ https://www.e-education.psu.edu/geog485/node/17
+ http://kimberly.uidaho.edu/water/fao56/fao56.pdf
+ https://pcjericks.github.io/py-gdalogr-cookbook/
+ https://docs.python.org/3/reference/index.html

# Bibliografia Complementar
1. https://pythongisresources.wordpress.com/2015/11/23/geoprocessing-with-python/
+ https://pythongisresources.wordpress.com/2015/11/23/python-geospatial-analysis-cookbook/
+ https://pythongisresources.wordpress.com/2015/08/29/python-geospatial-analysis-essentials/
+ https://pythongisresources.wordpress.com/2015/06/23/python-geospatial-development-essentials/
+ https://pythongisresources.wordpress.com/2015/04/15/learning-geospatial-analysis-with-python/
+ https://pythongisresources.wordpress.com/2015/04/15/python-geospatial-development/
